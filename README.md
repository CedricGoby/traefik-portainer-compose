# Traefik Portainer Compose

Ce dépôt contient : 
  * Un script d'installation de [Traefik](https://github.com/traefik/traefik) et [Portainer](https://github.com/portainer/portainer) sur un serveur Linux : [install-traefik-portainer.sh](install-traefik-portainer.sh)
  * Des fichiers exemple "Docker compose" à déployer avec Portainer : [apps](apps)

## Prérequis

  * Vous devez disposer d'un nom de domaine (ex mon-domaine.org) et de deux sous-domaines : traefik.mon-domaine.org et portainer.mon-domaine.org.
  * Pour chiffrer le mot de passe du compte "admin" de Traefik le script utilise la commande htpasswd issue du paquet apache2-utils. Pour installer ce paquet (Debian, Ubuntu) :<br>
`apt install apache2-utils`

## Déploiement de traefik & portainer

  * Cloner le dépôt<br>
  `git clone https://gitlab.com/CedricGoby/traefik-portainer-compose.git`

  * Exécuter le script d'installation<br>
`cd traefik-portainer-compose`<br>
`./install-traefik-portainer.sh`

  * Lors de l'installation renseignez les éléments suivants :
    * Nom de domaine (ex mon-domaine.org)
    * [Email d'expiration](https://letsencrypt.org/fr/docs/expiration-emails/) pour Let's Encrypt
    * Mot de passe pour le compte "admin" de Traefik

Environ une minute après la fin de l’exécution du script traefik et portainer sont accessibles via https://traefik.mon-domaine.org et https://portainer.mon-domaine.org respectivement.  

**Il faut immédiatement se rendre sur https://portainer.mon-domaine.org pour créer le mot de passe administrateur.**

# RESSOURCES

[Easy container management with Docker Compose Traefik v2](https://rafrasenberg.com/posts/docker-container-management-with-traefik-v2-and-portainer/)
